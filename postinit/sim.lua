local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
IAENV.AddSimPostInit(function()

    -- Map is not a proper component, so we edit it here instead.

    local function quantizepos(pt)
        local x, y, z = TheWorld.Map:GetTileCenterPoint(pt:Get())

        if pt.x > x then
            x = x + 1
        else
            x = x - 1
        end

        if pt.z > z then
            z = z + 1
        else
            z = z - 1
        end

        return Vector3(x, y, z)
    end

    local CANT_PLANT_TILES = {
        [WORLD_TILES.MAGMAFIELD] = true,
        [WORLD_TILES.VOLCANO_ROCK] = true,
        [WORLD_TILES.VOLCANO] = true,
        [WORLD_TILES.ASH] = true,
    }
    local function can_plant_at_point(x, y, z, allow_water)
        local tile = GetVisualTileType(x, y, z)
        if CANT_PLANT_TILES[tile] then
            return false
        end
        return allow_water or not IsWater(tile)
    end

    local _CanPlantAtPoint = Map.CanPlantAtPoint
    function Map:CanPlantAtPoint(x, y, z)
        if not can_plant_at_point(x, y, z) then
            return false
        end
        return _CanPlantAtPoint(self, x, y, z)
    end

    local BASE_TILES = {
        [WORLD_TILES.VOLCANO_ROCK] = true,
        [WORLD_TILES.BEACH] = true,
    }
    local _CanPlaceTurfAtPoint = Map.CanPlaceTurfAtPoint
    function Map:CanPlaceTurfAtPoint(x, y, z, ...)
        return _CanPlaceTurfAtPoint(self, x, y, z, ...) or BASE_TILES[self:GetTileAtPoint(x, y, z)]
    end

    local _CanDeployAtPoint = Map.CanDeployAtPoint
    function Map:CanDeployAtPoint(pt, inst, ...)
        if inst.prefab == "tar" then
            pt = quantizepos(pt)
            return _CanDeployAtPoint(self, pt, inst, ...) and not IsOnWater(pt, nil, nil, nil, .4) -- extra shore spacing
        end
        return _CanDeployAtPoint(self, pt, inst, ...)
    end

    local _CanDeployAtPointInWater = Map.CanDeployAtPointInWater
    function Map:CanDeployAtPointInWater(pt, inst, mouseover, data, ...)
        if inst.prefab == "boat_item" then
            local players = FindPlayersInRange(pt.x, pt.y, pt.z, 5)  -- Dst boat can't deploy in sailing player
            for _, player in ipairs(players) do
                if player and player:HasTag("sailing") then
                    return false
                end
            end
        end

        return  _CanDeployAtPointInWater(self, pt, inst, mouseover, data, ...)
    end

    -- Placing on ocean or not
    local _CanDeployRecipeAtPoint = Map.CanDeployRecipeAtPoint
    function Map:CanDeployRecipeAtPoint(pt, recipe, rot, player)
        local candeploy = _CanDeployRecipeAtPoint(self, pt, recipe, rot, player)
        local test_valid = (not recipe.testfn or recipe.testfn(pt, rot)) and Map:IsDeployPointClear(pt, nil, recipe.min_spacing or 3.2)
        if not test_valid or (not candeploy and not recipe.aquatic) then
            return false
        end

        local pt_x, pt_y, pt_z = pt:Get()
        if recipe.aquatic then
            local platform_ents = TheSim:FindEntities(pt_x, pt_y, pt_z, TUNING.MAX_WALKABLE_PLATFORM_RADIUS + TUNING.BOAT.NO_BUILD_BORDER_RADIUS + 1, {"walkableplatform"})
            for i, v in ipairs(platform_ents) do
                if v.components.walkableplatform and math.sqrt(v:GetDistanceSqToPoint(pt_x, 0, pt_z)) <= v.components.walkableplatform.platform_radius + (recipe.aquatic.platform_buffer_min or 0) then
                    return false
                end
            end
        end  --Prevent aquatic build on dst boat

        local tile = WORLD_TILES.GRASS
        local map = TheWorld.Map;
        tile = map:GetTileAtPoint(pt:Get())

        -- how the heck do i figure this out?
        local px, py, pz
        if player ~= nil then
            px, py, pz = player.Transform:GetWorldPosition()
        end
        local boating
        local platform
        if px and py and pz then
            boating = IsOnWater(px, py, pz)
            platform = map:GetPlatformAtPoint(px,py,pz)
        end

        if tile == WORLD_TILES.IMPASSABLE or (boating and not (recipe.aquatic or BUILDMODE.WATER == recipe.build_mode)) then
            return false
        end
        if recipe.aquatic then
            local x, y, z = pt:Get()
			local IsWaterMode = IA_CONFIG.aquaticplacedstwater and IsWaterAny or IsWater  --true can place dst mod
            if boating or platform then
                if platform and platform.components.walkableplatform and math.sqrt(platform:GetDistanceSqToPoint(x, 0, z)) > platform.components.walkableplatform.platform_radius + (recipe.aquatic.platform_buffer_max or 0.5) + 1.3 then --1.5 is closer but some distance should be cut for ease of use
                    return false
                end
                local minBuffer = recipe.aquatic.aquatic_buffer_min or 2
                return IsWaterMode(tile) and IsWaterMode(map:GetTileAtPoint(x + minBuffer, y, z)) and
                           IsWaterMode(map:GetTileAtPoint(x - minBuffer, y, z)) and
                           IsWaterMode(map:GetTileAtPoint(x, y, z + minBuffer)) and
                           IsWaterMode(map:GetTileAtPoint(x, y, z - minBuffer))
            else
                if recipe.aquatic.noshore then --used by the ballphinhouse
                    return false
                end

                --atm a lot of none shore waters can spawn near the shore so make all water count as a shoreline for now -Half
                if not --[[IsShore(GetVisualTileType(x, y, z))--]] IsWaterAny(GetVisualTileType(x, y, z)) then
                    return false
                end

                local maxBuffer = recipe.aquatic.shore_buffer_max or 2

                if not ((not IsWaterMode(GetVisualTileType(x + maxBuffer, y, z))) or
                    (not IsWaterMode(GetVisualTileType(x - maxBuffer, y, z))) or
                    (not IsWaterMode(GetVisualTileType(x, y, z + maxBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x, y, z - maxBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x + maxBuffer, y, z + maxBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x - maxBuffer, y, z + maxBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x + maxBuffer, y, z - maxBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x - maxBuffer, y, z - maxBuffer)))) then
                    return false
                end

                local minBuffer = recipe.aquatic.shore_buffer_min or 0.5

                if ((not IsWaterMode(GetVisualTileType(x + minBuffer, y, z))) or
                    (not IsWaterMode(GetVisualTileType(x - minBuffer, y, z))) or
                    (not IsWaterMode(GetVisualTileType(x, y, z + minBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x, y, z - minBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x + minBuffer, y, z + minBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x - minBuffer, y, z + minBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x + minBuffer, y, z - minBuffer))) or
                    (not IsWaterMode(GetVisualTileType(x - minBuffer, y, z - minBuffer)))) then
                    return false
                end

                return true
            end
        end

        return candeploy
    end

    function Map:CanVolcanoPlantAtPoint(x, y, z)
        local tile = self:GetTileAtPoint(x, y, z)
        return tile == WORLD_TILES.MAGMAFIELD or tile == WORLD_TILES.ASH or tile == WORLD_TILES.VOLCANO
    end

    local function can_deploy_on_water(inst)
        local deployable = inst.components.deployable
        if deployable then
            return deployable.candeployonshallowocean
                or deployable.candeployonbuildableocean
                or deployable.candeployonunbuildableocean
        end
        local invitem = inst.replica.inventoryitem
        if invitem then
            return invitem.classified.candeployonshallowocean:value()
                or invitem.classified.candeployonbuildableocean:value()
                or invitem.classified.candeployonunbuildableocean:value()
        end
    end

    local _CanDeployPlantAtPoint = Map.CanDeployPlantAtPoint
    function Map:CanDeployPlantAtPoint(pt, inst, ...)
        if inst:HasTag("volcanicplant") then
            return self:CanVolcanoPlantAtPoint(pt:Get())
                and self:IsDeployPointClear(pt, inst, inst.replica.inventoryitem ~= nil and inst.replica.inventoryitem:DeploySpacingRadius() or DEPLOYSPACING_RADIUS[DEPLOYSPACING.DEFAULT])
        elseif can_deploy_on_water(inst) then
            local x, y, z = pt:Get()
            return can_plant_at_point(x, y, z, true)
                and self:IsDeployPointClear(pt, inst, inst.replica.inventoryitem ~= nil and inst.replica.inventoryitem:DeploySpacingRadius() or DEPLOYSPACING_RADIUS[DEPLOYSPACING.DEFAULT])
        else
            return _CanDeployPlantAtPoint(self, pt, inst, ...)
        end
    end

    local _CanDeployWallAtPoint = Map.CanDeployWallAtPoint
    function Map:CanDeployWallAtPoint(pt, inst, ...)

        for i, v in ipairs(TheSim:FindEntities(pt.x, 0, pt.z, 2, {"sandbag"})) do
            if v ~= inst and v.entity:IsVisible() and v.components.placer == nil and v.entity:GetParent() == nil then
                local opt = v:GetPosition()
                -- important to remove sign in order to calculate accuracte distance
                if math.abs(math.abs(opt.x) - math.abs(pt.x)) < 1 and math.abs(math.abs(opt.z) - math.abs(pt.z)) < 1 then
                    return false
                end
            end
        end

        return _CanDeployWallAtPoint(self, pt, inst, ...)
    end

    function Map:GetClosestTileDist(x, y, z, tile, radius)
        x, y = self:GetTileXYAtPoint(x, y, z)
        for r = 1, radius do
            if tile == self:GetTile(x - r, y) or tile == self:GetTile(x + r, y) or tile == self:GetTile(x, y - r) or tile == self:GetTile(x, y + r) then
                return r
            end

            for i = 1, r - 1 do
                if tile == self:GetTile(x + r, y + i) or tile == self:GetTile(x + r, y - i) or tile == self:GetTile(x - r, y + i) or tile == self:GetTile(x - r, y - i)
                    or tile == self:GetTile(x + i, y + r) or tile == self:GetTile(x + i, y - r) or tile == self:GetTile(x - i, y + r) or tile == self:GetTile(x - i, y - r)
                then
                    return math.sqrt(r * r + i * i)
                end
            end
            if tile == self:GetTile(x + r, y + r) or tile == self:GetTile(x + r, y - r) or tile == self:GetTile(x - r, y + r) or tile == self:GetTile(x - r, y - r) then
                return math.sqrt(2) * r
            end
        end

        return radius + 1
    end

    ---------------------------------------------------------------------------------------------------------------------------------------------

    for k, v in pairs(IA_VEGGIES) do
        table.insert(Prefabs.plant_normal.assets, Asset("ANIM", "anim/" .. k))
        table.insert(Prefabs.plant_normal.deps, k)
        table.insert(Prefabs.seeds.deps, k)
        VEGGIES[k] = v
        if v.seed_weight then
            TUNING.BURNED_LOOT_OVERRIDES[k .. "_seeds"] = "seeds_cooked"
        end
    end

    ----------------------------------------------------------------------------------------------------------------------------------------

    function RunAway:GetRunAngle(pt, hp)
        if self.avoid_angle ~= nil then
            local avoid_time = GetTime() - self.avoid_time
            if avoid_time < 1 then
                return self.avoid_angle
            else
                self.avoid_time = nil
                self.avoid_angle = nil
            end
        end

        local angle = self.inst:GetAngleToPoint(hp) + 180 -- + math.random(30)-15
        if angle > 360 then
            angle = angle - 360
        end

        -- print(string.format("RunAway:GetRunAngle me: %s, hunter: %s, run: %2.2f", tostring(pt), tostring(hp), angle))

        local radius = 6

        local result_offset, result_angle, deflected = FindWalkableOffset(pt, angle * DEGREES, radius, 8, true, false, IsPositionValidForEnt(self.inst, 2)) -- try avoiding walls
        if result_angle == nil then
            result_offset, result_angle, deflected = FindWalkableOffset(pt, angle * DEGREES, radius, 8, true, true, IsPositionValidForEnt(self.inst, 2)) -- ok don't try to avoid walls, but at least avoid water
            if result_angle == nil then
                return angle -- ok whatever, just run
            end
        end

        result_angle = result_angle / DEGREES
        if deflected then
            self.avoid_time = GetTime()
            self.avoid_angle = result_angle
        end
        return result_angle
    end

    function Wander:PickNewDirection()
        self.far_from_home = self:IsFarFromHome()

        self.walking = true

        if self.far_from_home then
            -- print("Far from home, going back")
            -- print(self.inst, Point(self.inst.Transform:GetWorldPosition()), "FAR FROM HOME", self:GetHomePos())
            self.inst.components.locomotor:GoToPoint(self:GetHomePos())
        else
            local pt = Point(self.inst.Transform:GetWorldPosition())
            local angle = (self.getdirectionFn and self.getdirectionFn(self.inst))
            -- print("got angle ", angle)
            if not angle then
                angle = math.random() * 2 * PI
                -- print("no angle, picked", angle, self.setdirectionFn)
                if self.setdirectionFn then
                    -- print("set angle to ", angle)
                    self.setdirectionFn(self.inst, angle)
                end
            end

            local radius = 12
            local attempts = 8
            local offset, check_angle, deflected = FindWalkableOffset(pt, angle, radius, attempts, true, false, IsPositionValidForEnt(self.inst, 2)) -- try to avoid walls
            if not check_angle then
                -- print(self.inst, "no los wander, fallback to ignoring walls")
                offset, check_angle, deflected = FindWalkableOffset(pt, angle, radius, attempts, true, true, IsPositionValidForEnt(self.inst, 2)) -- if we can't avoid walls, at least avoid water
            end
            if check_angle then
                angle = check_angle
                if self.setdirectionFn then
                    -- print("(second case) reset angle to ", angle)
                    self.setdirectionFn(self.inst, angle)
                end
            else
                -- guess we don't have a better direction, just go whereever
                -- print(self.inst, "no walkable wander, fall back to random")
            end
            -- print(self.inst, pt, string.format("wander to %s @ %2.2f %s", tostring(offset), angle/DEGREES, deflected and "(deflected)" or ""))
            if offset then
                self.inst.components.locomotor:GoToPoint(self.inst:GetPosition() + offset)
            else
                self.inst.components.locomotor:WalkInDirection(angle / DEGREES)
            end
        end

        self:Wait(self.times.minwalktime + math.random() * self.times.randwalktime)
    end

    -------------------------- RANDOM TESTS ----------------------------------------

    -- CurrentFnToDebug = nil
    -- currentDebugLocals = nil
    -- currentDebugUpvals = nil

    -- mydebuggetstatus = "waiting"

    -- function SetFnToDebug(fn, base)
    --  CurrentFnToDebug = base and getmetatable(base).__index.fn or fn
    --  if CurrentFnToDebug ~= nil then
    --    mydebuggetstatus = "ready"
    --  end
    -- end

    -- function MyDebugGetLocal()
    --  if mydebuggetstatus == "paused" then
    --    return
    --  end

    --  local funcInf = debug.getinfo(2)

    ----  print("------------------HOOK------------------")
    ----  for k, v in pairs(funcInf) do
    ----    print(k,"=",v)
    ----  end

    ----  print("Hook fn for ", funcInf.func, "/ Currently tracked function:", CurrentFnToDebug)

    --  currentDebugLocals = nil
    --  currentDebugUpvals = nil

    --  if CurrentFnToDebug ~= nil and funcInf.func == CurrentFnToDebug then
    --    currentDebugLocals={}
    --    local i = 1
    --    while true do
    --      local n, v = debug.getlocal(2, i)
    --      if not n then break end
    ----      print(tostring(n).." = "..tostring(v))
    --      table.insert(currentDebugLocals, {name = n, value = v})
    --      i = i + 1
    --    end

    --    currentDebugUpvals={}
    --    i = 1
    --    while true do
    --      local n, v = debug.getupvalue (funcInf.func, i)
    --      if not n then break end
    ----      print(tostring(n).." = "..tostring(v))
    --      table.insert(currentDebugUpvals, {name = n, value = v})
    --      i = i + 1
    --    end

    --    mydebuggetstatus = "paused"
    --  end
    -- end

    -- debug.sethook(MyDebugGetLocal, "c")

    if TheWorld:HasTag("forest") and (not TheWorld.meta.patcher or not TheWorld.meta.patcher.spawnedsunken_boat) then  -- Put it here for the time being
        local sunken_boat = nil
        for i, node in ipairs(TheWorld.topology.nodes) do
            if node.type == NODE_TYPE.Background then
                local x = node.cent[1]
                local z = node.cent[2]

                for r = 15, 60, 15 do
                    local offset = FindValidPositionByFan(0, r, 20, function(o)
                            local pos = Vector3(o.x+x, 0, o.z+z)
                            -- first, we need water
                            if not TheWorld.Map:IsOceanTileAtPoint(pos.x, pos.y, pos.z) then
                                return false
                            end
                            -- second, it should be surrounded by water (no rivers plz)
                            local land = FindValidPositionByFan(0, 10, 8, function(o2)
                                return not TheWorld.Map:IsOceanTileAtPoint(pos.x + o2.x, 0, pos.z + o2.z)
                            end)
                            if land ~= nil then return false end
                            return true
                        end)
                    if offset ~= nil then
                        -- We found ocean! now walk towards land and put us on the beach.
                        local pos = Vector3(offset.x+x, 0, offset.z+z)

                        local dir = Vector3(-offset.x, -offset.y, -offset.z):Normalize()
                        while math.abs(pos.x - x) > 1 and math.abs(pos.z - z) > 1 do
                            local tilepos = Vector3(TheWorld.Map:GetTileCenterPoint(pos.x, pos.y, pos.z))
                            if not TheWorld.Map:IsOceanTileAtPoint(tilepos.x, tilepos.y, tilepos.z) then
                                -- first clean up the area
                                -- local ents = TheSim:FindEntities(tilepos.x, tilepos.y, tilepos.z, 6)
                                -- for i,v in ipairs(ents) do
                                --     v:DoTaskInTime(0, v.Remove)
                                -- end

                                -- then spawn a boat!
                                sunken_boat = SpawnPrefab("sunken_boat")
                                sunken_boat.Transform:SetPosition(tilepos:Get())
                                print("Spawned the sunken_boat.")
                                break
                            end

                            pos = pos + dir
                        end
                    end
                    if sunken_boat then
                        break
                    end
                end
            end
            if sunken_boat then
                break
            end
        end
        if sunken_boat then
            if TheWorld.meta.patcher == nil then
                TheWorld.meta.patcher = {}
            end
            TheWorld.meta.patcher.spawnedsunken_boat = true

            -- Put some flotsam near the wreck for dramatic effect
            local offsets = {
                Vector3(1, 0, 0),
                Vector3(1, 0, 1),
                Vector3(0, 0, 1),
                Vector3(-1, 0, 1),
                Vector3(-1, 0, 0),
                Vector3(-1, 0, -1),
                Vector3(0, 0, -1),
                Vector3(1, 0, -1),
            }
            shuffleArray(offsets)
            local numspawned = 0
            local pos = Vector3(sunken_boat.Transform:GetWorldPosition())
            for i=1, #offsets do
                local offset = offsets[i] * 12
                local land = FindValidPositionByFan(0, 6, 8, function(o)
                    return not TileGroupManager:IsOceanTile(GetGroundTypeAtPosition(pos + offset + o))
                end)
                if not land then
                    local flotsam = SpawnPrefab("flotsam")
                    flotsam.Transform:SetPosition((pos + offset):Get())
                    flotsam.components.drifter:SetDriftTarget(pos)
                    numspawned = numspawned + 1
                end
                if numspawned == 3 then
                    break
                end
            end
        else
            print("UH OH! We couldn't find a spot in the world for the sunken_boat!")
        end
    end

end)
