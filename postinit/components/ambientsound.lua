local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddComponentPostInit("ambientsound", function(cmp)
	--local WAVE_SOUNDS = UpvalueHacker.GetUpvalue(cmp.OnUpdate, "WAVE_SOUNDS")
    local AMBIENT_SOUNDS = UpvalueHacker.GetUpvalue(cmp.OnUpdate, "AMBIENT_SOUNDS")
	local _worldstate = TheWorld.state

    local _isIAClimate = false
    local _isDSTClimate = false
    local _isVolcanoClimate = false

    AMBIENT_SOUNDS[ WORLD_TILES.JUNGLE ] = {
    	sound = "ia/amb/mild/jungleAMB", 
		mildsound="ia/amb/mild/jungleAMB",
		wetsound="ia/amb/wet/jungleAMB",
		greensound="ia/amb/green/jungleAMB",
		drysound="ia/amb/dry/jungleAMB",
    	rainsound = "ia/amb/rain/jungleAMB", 
    	hurricanesound = "ia/amb/hurricane/jungleAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.BEACH ] = {
    	sound = "ia/amb/mild/beachAMB", 
		mildsound="ia/amb/mild/beachAMB", 
		wetsound="ia/amb/wet/beachAMB", 
		greensound="ia/amb/green/beachAMB", 
		drysound="ia/amb/dry/beachAMB",
    	rainsound = "ia/amb/rain/beachAMB", 
    	hurricanesound = "ia/amb/hurricane/beachAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.SWAMP ] = { --NOTE: Unused
    	sound = "ia/amb/mild/marshAMB", 
		mildsound="ia/amb/mild/marshAMB", 
		wetsound="ia/amb/wet/marshAMB", 
		greensound="ia/amb/green/marshAMB", 
		drysound="ia/amb/dry/marshAMB",
    	rainsound = "ia/amb/rain/marshAMB", 
    	hurricanesound = "ia/amb/hurricane/marshAMB",
    }
	AMBIENT_SOUNDS[ WORLD_TILES.RIVER ] = AMBIENT_SOUNDS[ WORLD_TILES.SWAMP ]
    AMBIENT_SOUNDS[ WORLD_TILES.MAGMAFIELD ] = {
    	sound = "ia/amb/mild/rockyAMB", 
		mildsound="ia/amb/mild/rockyAMB", 
		wetsound="ia/amb/wet/rockyAMB", 
		greensound="ia/amb/green/rockyAMB", 
		drysound="ia/amb/dry/rockyAMB",
    	rainsound = "ia/amb/rain/rockyAMB", 
    	hurricanesound = "ia/amb/hurricane/rockyAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.TIDALMARSH ] = {
    	sound = "ia/amb/mild/marshAMB", 
		mildsound="ia/amb/mild/marshAMB", 
		wetsound="ia/amb/wet/marshAMB", 
		greensound="ia/amb/green/marshAMB", 
		drysound="ia/amb/dry/marshAMB",
    	rainsound = "ia/amb/rain/marshAMB", 
    	hurricanesound = "ia/amb/hurricane/marshAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.MEADOW ] = {
    	sound = "ia/amb/mild/grasslandAMB", 
		wintersound="dontstarve/AMB/grassland_winter", 
		springsound="dontstarve/AMB/grassland", 
		summersound="dontstarve_DLC001/AMB/grassland_summer", 
		rainsound="dontstarve/AMB/grassland_rain", 
		mildsound="ia/amb/mild/grasslandAMB", 
		wetsound="ia/amb/wet/grasslandAMB", 
		greensound="ia/amb/green/grasslandAMB", 
		drysound="ia/amb/dry/grasslandAMB",
    	rainsound = "ia/amb/rain/grasslandAMB", 
    	hurricanesound = "ia/amb/hurricane/grasslandAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_SHALLOW ] = {
    	sound = "ia/amb/mild/ocean_shallow", 
		mildsound="ia/amb/mild/ocean_shallow", 
		wetsound="ia/amb/wet/ocean_shallowAMB", 
		greensound="ia/amb/green/ocean_shallowAMB", 
		drysound="ia/amb/dry/ocean_shallow",
    	wintersound = "ia/amb/wet/ocean_shallowAMB", 
    	springsound = "ia/amb/green/ocean_shallowAMB", 
    	summersound = "ia/amb/dry/ocean_shallow", 
    	rainsound = "ia/amb/rain/ocean_shallowAMB", 
    	hurricanesound = "ia/amb/hurricane/ocean_shallowAMB",
    }
	AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_SHALLOW_SHORE ] = {
    	sound = "ia/amb/mild/waves", 
    	mildsound="ia/amb/mild/waves", 
		wetsound="ia/amb/wet/waves", 
		greensound="ia/amb/green/waves", 
		drysound="ia/amb/dry/waves", 
		rainsound="ia/amb/rain/waves", 
		hurricanesound="ia/amb/hurricane/waves",
    }
	AMBIENT_SOUNDS[ WORLD_TILES.RIVER_SHORE ] = AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_SHALLOW_SHORE ]
    AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_MEDIUM ] = {
    	sound = "ia/amb/mild/ocean_shallow", 
		mildsound="ia/amb/mild/ocean_shallow", 
		wetsound="ia/amb/wet/ocean_shallowAMB", 
		greensound="ia/amb/green/ocean_shallowAMB", 
		drysound="ia/amb/dry/ocean_shallow",
    	rainsound = "ia/amb/rain/ocean_shallowAMB", 
    	hurricanesound = "ia/amb/hurricane/ocean_shallowAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_DEEP ] = {
    	sound = "ia/amb/mild/ocean_deep", 
		mildsound="ia/amb/mild/ocean_deep", 
		wetsound="ia/amb/wet/ocean_deepAMB", 
		greensound="ia/amb/green/ocean_deepAMB", 
		drysound="ia/amb/dry/ocean_deep",
    	rainsound = "ia/amb/rain/ocean_deepAMB", 
    	hurricanesound = "ia/amb/hurricane/ocean_deepAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_SHIPGRAVEYARD ] = {
    	sound = "ia/amb/mild/ocean_deep", 
    	mildsound="ia/amb/mild/ocean_deep", 
		wetsound="ia/amb/wet/ocean_deepAMB", 
		greensound="ia/amb/green/ocean_deepAMB", 
		drysound="ia/amb/dry/ocean_deep",
    	rainsound = "ia/amb/rain/ocean_deepAMB", 
    	hurricanesound = "ia/amb/hurricane/ocean_deepAMB",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_CORAL ] = {
    	sound = "ia/amb/mild/coral_reef", 
		mildsound="ia/amb/mild/coral_reef", 
		wetsound="ia/amb/wet/coral_reef", 
		greensound="ia/amb/green/coral_reef", 
		drysound="ia/amb/dry/coral_reef",
    	rainsound = "ia/amb/rain/coral_reef", 
    	hurricanesound = "ia/amb/hurricane/coral_reef",
    }
	AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_CORAL_SHORE ] = AMBIENT_SOUNDS[ WORLD_TILES.OCEAN_CORAL ]
    AMBIENT_SOUNDS[ WORLD_TILES.MANGROVE ] = {
    	sound = "ia/amb/mild/mangrove", 
		mildsound="ia/amb/mild/mangrove", 
		wetsound="ia/amb/wet/mangrove", 
		greensound="ia/amb/green/mangrove", 
		drysound="ia/amb/dry/mangrove",
    	rainsound = "ia/amb/rain/mangrove", 
    	hurricanesound = "ia/amb/hurricane/mangrove",
    }
	AMBIENT_SOUNDS[ WORLD_TILES.MANGROVE ] = AMBIENT_SOUNDS[ WORLD_TILES.MANGROVE_SHORE ]
    AMBIENT_SOUNDS[ WORLD_TILES.RIVER ] = {
    	sound = "ia/amb/mild/waves", 
		mildsound = "ia/amb/mild/waves",
    	wintersound = "ia/amb/wet/waves", 
    	springsound = "ia/amb/green/waves", 
    	summersound = "ia/amb/dry/waves", 
    	rainsound = "ia/amb/rain/waves", 
    	hurricanesound = "ia/amb/hurricane/waves",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.VOLCANO ] = {
    	sound = "ia/amb/volcano/ground_ash", 
    	dormantsound = "ia/amb/volcano/dormant", 
    	activesound = "ia/amb/volcano/active",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.VOLCANO_ROCK ] = {
    	sound = "ia/amb/volcano/ground_ash", 
    	dormantsound = "ia/amb/volcano/dormant", 
    	activesound = "ia/amb/volcano/active",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.VOLCANO_LAVA ] = {
    	sound = "ia/amb/volcano/lava",
    }
    AMBIENT_SOUNDS[ WORLD_TILES.ASH ] = {
    	sound = "ia/amb/volcano/ground_ash", 
    	dormantsound = "ia/amb/volcano/dormant", 
    	activesound = "ia/amb/volcano/active",
    }


	-- SW season sounds for dst tiles --
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].mildsound="ia/amb/mild/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].wetsound="ia/amb/wet/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].greensound="ia/amb/green/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].drysound="ia/amb/dry/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].hurricanesound="ia/amb/hurricane/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].dormantsound="ia/amb/volcano/volano_alt"
	AMBIENT_SOUNDS[ WORLD_TILES.ROAD ].activesound="ia/amb/volcano/volano_alt"

	AMBIENT_SOUNDS[ WORLD_TILES.SAVANNA ].mildsound="ia/amb/mild/grasslandAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SAVANNA ].wetsound="ia/amb/wet/grasslandAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SAVANNA ].greensound="ia/amb/green/grasslandAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SAVANNA ].drysound="ia/amb/dry/grasslandAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SAVANNA ].hurricanesound="ia/amb/hurricane/grasslandAMB"

	AMBIENT_SOUNDS[ WORLD_TILES.BRICK_GLOW ].dormantsound="ia/amb/volcano/volano_alt"
	AMBIENT_SOUNDS[ WORLD_TILES.BRICK_GLOW ].wetsound="ia/amb/wet/grasslandAMB"
	--

	-- additional IA season sounds for dst tiles --
    AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].mildsound="ia/amb/mild/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].wetsound="ia/amb/wet/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].greensound="ia/amb/green/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].drysound="ia/amb/dry/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].hurricanesound="ia/amb/hurricane/rockyAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].dormantsound="ia/amb/volcano/volano_alt"
	AMBIENT_SOUNDS[ WORLD_TILES.ROCKY ].activesound="ia/amb/volcano/volano_alt"

	AMBIENT_SOUNDS[ WORLD_TILES.METEOR ].dormantsound="ia/amb/volcano/volano_alt"
	AMBIENT_SOUNDS[ WORLD_TILES.METEOR ].activesound="ia/amb/volcano/volano_alt"

	AMBIENT_SOUNDS[ WORLD_TILES.SHELLBEACH ].mildsound="ia/amb/mild/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SHELLBEACH ].wetsound="ia/amb/wet/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SHELLBEACH ].greensound="ia/amb/green/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SHELLBEACH ].drysound="ia/amb/dry/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.SHELLBEACH ].hurricanesound="ia/amb/hurricane/beachAMB"

	AMBIENT_SOUNDS[ WORLD_TILES.MONKEY_GROUND ].mildsound="ia/amb/mild/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.MONKEY_GROUND ].wetsound="ia/amb/wet/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.MONKEY_GROUND ].greensound="ia/amb/green/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.MONKEY_GROUND ].drysound="ia/amb/dry/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.MONKEY_GROUND ].hurricanesound="ia/amb/hurricane/beachAMB"

	AMBIENT_SOUNDS[ WORLD_TILES.PEBBLEBEACH ].mildsound="ia/amb/mild/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.PEBBLEBEACH ].wetsound="ia/amb/wet/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.PEBBLEBEACH ].greensound="ia/amb/green/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.PEBBLEBEACH ].drysound="ia/amb/dry/beachAMB"
	AMBIENT_SOUNDS[ WORLD_TILES.PEBBLEBEACH ].hurricanesound="ia/amb/hurricane/beachAMB"
	--

	local _AMBIENT_SOUNDS = {}
	local _activatedplayer
	local function onSoundsDirty()
		if _activatedplayer then
			--reset
			for tile,sounds in pairs(_AMBIENT_SOUNDS) do
				AMBIENT_SOUNDS[tile] = sounds
			end
			_AMBIENT_SOUNDS = deepcopy(AMBIENT_SOUNDS)

			local climate = GetClimate(_activatedplayer)
			_isIAClimate = IsIAClimate(climate)
			_isDSTClimate = IsDSTClimate(climate)
			_isVolcanoClimate = IsClimate(climate, "volcano")

			--set
			for tile,sounds in pairs(AMBIENT_SOUNDS) do
				if type(sounds) == "table" then
					if _isIAClimate then
						local hurricane = _worldstate.hurricane and sounds.hurricanesound or nil
						local dormant = _isVolcanoClimate and sounds.dormantsound or nil
						local active = _isVolcanoClimate and sounds.activesound or nil

						AMBIENT_SOUNDS[tile].rainsound = not (dormant or active) and (hurricane or sounds.rainsound) or nil
						AMBIENT_SOUNDS[tile].sound = dormant or hurricane or sounds.mildsound or sounds.sound
						AMBIENT_SOUNDS[tile].wintersound = not (active or hurricane) and (sounds.wetsound or sounds.wintersound) or nil
						AMBIENT_SOUNDS[tile].springsound = not (active or hurricane) and (sounds.greensound or sounds.springsound) or nil
						AMBIENT_SOUNDS[tile].summersound = active or not hurricane and (sounds.drysound or sounds.summersound) or nil --vm is active during summer/dry
					else
						AMBIENT_SOUNDS[tile].sound = sounds.sound or sounds.mildsound
						AMBIENT_SOUNDS[tile].wintersound = sounds.wintersound or sounds.wetsound
						AMBIENT_SOUNDS[tile].springsound = sounds.springsound or sounds.greensound
						AMBIENT_SOUNDS[tile].summersound = sounds.summersound or sounds.drysound					
					end

					if _isVolcanoClimate then
						cmp:SetWavesEnabled(false)
					else
						cmp:SetWavesEnabled(true)
					end
				end
			end
		end
	end

	cmp.inst:ListenForEvent("playeractivated", function(src, player)
    	if player and _activatedplayer ~= player then
    		player:ListenForEvent("climatechange", onSoundsDirty)
    		player:DoTaskInTime(0, onSoundsDirty) --initialise
    	end
    	_activatedplayer = player
    end)
    cmp.inst:ListenForEvent("playerdeactivated", function(src, player)
    	if player then
    		player:RemoveEventCallback("climatechange", onSoundsDirty)
    		if _activatedplayer == player then
    			_activatedplayer = nil
    		end
    	end
    end)
	cmp.inst:WatchWorldState("hurricanechanged", onSoundsDirty)
end)
