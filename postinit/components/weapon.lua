local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Weapon = require("components/weapon")

function Weapon:SetPoisonous() 
    self.stimuli = "poisonous" 
end

function Weapon:SetOnExtraAttack(fn)
    self.onextraattack = fn
end

local _OnAttack = Weapon.OnAttack
function Weapon:OnAttack(attacker, target, ...)
    if self.onextraattack then -- Like onattack but passes in all the args like in ds, used for weapons that call the onattack of another weapon (speargun)
        self.onextraattack(self.inst, attacker, target, ...)
    end
    
    _OnAttack(self, attacker, target, ...)
    
    if self.inst.components.obsidiantool then
        self.inst.components.obsidiantool:Use(attacker, target)
    end
end


IAENV.AddComponentPostInit("weapon", function(cmp)
    cmp.onextraattack = nil
end)
