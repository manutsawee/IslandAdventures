local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function fn(inst)

if TheWorld:HasTag("island") then

    inst.AnimState:SetOceanBlendParams(0)
	
end


end

IAENV.AddPrefabPostInit("float_fx_front", fn)
IAENV.AddPrefabPostInit("float_fx_back", fn)
