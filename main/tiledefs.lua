--[[
tile_name - the name of the tile, this is how you'll refer to your tile in the WORLD_TILES table.
tile_range - the string defining the range of possible ids for the tile.
the following ranges exist: "LAND", "NOISE", "OCEAN", "IMPASSABLE"
tile_data {
    [ground_name]
    [old_static_id] - optional, the static tile id that this tile had before migrating to this API, if you aren't migrating your tiles from an old API to this one, omit this.
}
ground_tile_def {
    [name] - this is the texture for the ground, it will first attempt to load the texture at "levels/texture/<name>.tex", if that fails it will then treat <name> as the whole file path for the texture.
    [atlas] - optional, if missing it will load the same path as name, but ending in .xml instead of .tex,  otherwise behaves the same as <name> but with .xml instead of .tex.
    [noise_texture] -  this is the noise texture for the ground, it will first attempt to load the texture at "levels/texture/<noise_texture>.tex", if that fails it will then treat <noise_texture> as the whole file path for the texture.
    [runsound] - soundpath for the run sound, if omitted will default to "dontstarve/movement/run_dirt"
    [walksound] - soundpath for the walk sound, if omitted will default to "dontstarve/movement/walk_dirt"
    [snowsound] - soundpath for the snow sound, if omitted will default to "dontstarve/movement/run_snow"
    [mudsound] - soundpath for the mud sound, if omitted will default to "dontstarve/movement/run_mud"
    [flashpoint_modifier] - the flashpoint modifier for the tile, defaults to 0 if missing
    [colors] - the colors of the tile when for blending of the ocean colours, will use DEFAULT_COLOUR(see tilemanager.lua for the exact values of this table) if missing.
    [flooring] - if true, inserts this tile into the GROUND_FLOORING table.
    [hard] - if true, inserts this tile into the GROUND_HARD table.
    [cannotbedug] - if true, inserts this tile into the TERRAFORM_IMMUNE table.
    other values can also be stored in this table, and can tested for via the GetTileInfo function.
}
minimap_tile_def {
    [name] - this is the texture for the minimap, it will first attempt to load the texture at "levels/texture/<name>.tex", if that fails it will then treat <name> as the whole file path for the texture.
    [atlas] - optional, if missing it will load the same path as name, but ending in .xml instead of .tex,  otherwise behaves the same as <name> but with .xml instead of .tex.
    [noise_texture] -  this is the noise texture for the minimap, it will first attempt to load the texture at "levels/texture/<noise_texture>.tex", if that fails it will then treat <noise_texture> as the whole file path for the texture.
}
turf_def {
    [name] - the postfix for the prefabname of the turf item
    [anim] - the name of the animation to play for the turf item, if undefined it will use name instead
    [bank_build] - the bank and build containing the animation, if undefined bank_build will use the value "turf"
}
-]]
local GroundTiles = require("worldtiledefs")
local NoiseFunctions = require("noisetilefunctions")
local TileGroupManager = GLOBAL.TileGroupManager
local TileGroups = GLOBAL.TileGroups

TileGroups.IAOceanTiles = TileGroupManager:AddTileGroup()

TileGroups.TEMP_LandTilesNotIAOceanTiles = TileGroupManager:AddTileGroup(TileGroups.LandTiles) --TEMP: remove when we merge our waters

local TileRanges =
{
    LAND = "LAND",
    NOISE = "NOISE",
    OCEAN = "OCEAN", --ROT
    -- HIGHOCEAN = "HIGHOCEAN", --DS STYLE ROT
    SEA = "SEA", --SW
    -- LAKE = "LAKE", --HAM
    IMPASSABLE = "IMPASSABLE",
}

local function volcano_noisefn(noise)
    return WORLD_TILES.VOLCANO_NOISE
end

local ia_tiledefs = {
    BEACH = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Beach",
            old_static_id = 90,
        },
        ground_tile_def  = {
            name = "beach",
            noise_texture = "ground_noise_sand",
            runsound = "dontstarve/movement/ia_run_sand",
            walksound = "dontstarve/movement/ia_walk_sand",
            flashpoint_modifier = 0,
            bank_build = "turf_ia",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_beach_noise",
        },
        --turf_def = {
        --    name = "beach",
        --    bank_build = "turf_ia",
        --},
    },
    MEADOW = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Beach",
            old_static_id = 91,
        },
        ground_tile_def  = {
            name = "jungle",
            noise_texture = "ground_noise_savannah_detail",
            runsound = "dontstarve/movement/run_tallgrass",
            walksound = "dontstarve/movement/walk_tallgrass",
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_savannah_noise",
        },
        turf_def = {
            name = "meadow",
            bank_build = "turf_ia",
        },
    },
    JUNGLE = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Jungle",
            old_static_id = 92,
        },
        ground_tile_def  = {
            name = "jungle",
            noise_texture = "ground_noise_jungle",
            runsound = "dontstarve/movement/run_woods",
            walksound = "dontstarve/movement/walk_woods",
            flashpoint_modifier = 0,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_jungle_noise",
        },
        turf_def = {
            name = "jungle",
            bank_build = "turf_ia",
        },
    },
    SWAMP = { --note this majestic creature is unused
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Swamp",
            old_static_id = 93,
        },
        ground_tile_def  = {
            name = "swamp",
            noise_texture = "ground_noise_swamp",
            runsound = "dontstarve/movement/run_marsh",
            walksound = "dontstarve/movement/walk_marsh",
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_swamp_noise",
        },
        turf_def = {
            name = "swamp",
            bank_build = "turf_ia",
        },
    },
    TIDALMARSH = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Tidal Marsh",
            old_static_id = 94,
        },
        ground_tile_def  = {
            name = "tidalmarsh",
            noise_texture = "ground_noise_tidalmarsh",
            runsound = "dontstarve/movement/run_marsh",
            walksound = "dontstarve/movement/walk_marsh",
            flashpoint_modifier = 0,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_tidalmarsh_noise",
        },
        turf_def = {
            name = "tidalmarsh",
            bank_build = "turf_ia",
        },
    },
    MAGMAFIELD = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Magmafield",
            old_static_id = 95,
        },
        ground_tile_def  = {
            name = "cave",
            noise_texture = "ground_noise_magmafield",
            runsound = "dontstarve/movement/run_slate",
            walksound = "dontstarve/movement/walk_slate",
            snowsound = "dontstarve/movement/run_ice",
            flashpoint_modifier = 0,
            hard = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_magmafield_noise",
        },
        turf_def = {
            name = "magmafield",
            bank_build = "turf_ia",
        },
    },
    VOLCANO_ROCK = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Volcanic Rock",
            old_static_id = 96,
        },
        ground_tile_def  = {
            name = "rocky",
            noise_texture = "ground_volcano_noise",
            runsound = "dontstarve/movement/run_rock",
            walksound = "dontstarve/movement/walk_rock",
            snowsound = "dontstarve/movement/run_ice",
            flashpoint_modifier = 0,
            hard = true,
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_ground_volcano_noise",
        },
    },
    VOLCANO = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Lava Rock",
            old_static_id = 97,
        },
        ground_tile_def  = {
            name = "cave",
            noise_texture = "ground_lava_rock",
            runsound = "dontstarve/movement/run_rock",
            walksound = "dontstarve/movement/walk_rock",
            snowsound = "dontstarve/movement/run_ice",
            flashpoint_modifier = 0,
            hard = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_ground_lava_rock",
        },
        turf_def = {
            name = "volcano",
            bank_build = "turf_ia",
        },
    },
    ASH = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Ash",
            old_static_id = 98,
        },
        ground_tile_def  = {
            name = "cave",
            noise_texture = "ground_ash",
            runsound = "dontstarve/movement/run_dirt",
            walksound = "dontstarve/movement/walk_dirt",
            snowsound = "dontstarve/movement/run_ice",
            flashpoint_modifier = 0,
            hard = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_ash",
        },
        turf_def = {
            name = "ash",
            bank_build = "turf_ia",
        },
    },
    SNAKESKIN = {
        tile_range = TileRanges.LAND,
        tile_data = {
            ground_name = "Snakeskin Carpet",
            old_static_id = 99,
        },
        ground_tile_def  = {
            name = "carpet",
            noise_texture = "noise_snakeskinfloor",
            runsound = "dontstarve/movement/run_carpet",
            walksound = "dontstarve/movement/walk_carpet",
            flashpoint_modifier = 0,
            floor = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "noise_snakeskinfloor",
        },
        turf_def = {
            name = "snakeskin",
            bank_build = "turf_ia",
        },
    },

    -------------------------------
    -- OCEAN/SEA/LAKE
    -- (after Land in order to keep render order consistent)
    -------------------------------

    RIVER_SHORE = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "River",
            old_static_id = 100,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_shallow",
            flashpoint_modifier = 250,
            is_shoreline = true,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_watershallow_noise",
        },
    },
    RIVER = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "River",
            old_static_id = 100,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_shallow",
            flashpoint_modifier = 250,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_watershallow_noise",
        },
    },
    MANGROVE_SHORE = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Mangrove",
            old_static_id = 106,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_water_mangrove",
            flashpoint_modifier = 250,
            is_shoreline = true,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_water_mangrove",
        },
    },
    MANGROVE = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Mangrove",
            old_static_id = 106,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_water_mangrove",
            flashpoint_modifier = 250,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_water_mangrove",
        },
    },
    OCEAN_SHALLOW_SHORE = { --was called OCEAN_SHORE in sw
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Shallow",
            old_static_id = 101,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_shallow",
            flashpoint_modifier = 250,
            is_shoreline = true,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_watershallow_noise",
        },
    },
    OCEAN_SHALLOW = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Shallow",
            old_static_id = 101,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_shallow",
            flashpoint_modifier = 250,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_watershallow_noise",
        },
    },
    OCEAN_CORAL_SHORE = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Coral",
            old_static_id = 104,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_water_coral",
            flashpoint_modifier = 250,
            is_shoreline = true,
            ocean_depth = "BASIC",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_water_coral",
        },
    },
    OCEAN_CORAL = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Coral",
            old_static_id = 104,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_water_coral",
            flashpoint_modifier = 250,
            ocean_depth = "BASIC",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_water_coral",
        },
    },
    OCEAN_MEDIUM = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Medium",
            old_static_id = 102,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_medium",
            flashpoint_modifier = 250,
            ocean_depth = "DEEP",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_watermedium_noise",
        },
    },
    OCEAN_DEEP = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Deep",
            old_static_id = 103,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_noise_water_deep",
            flashpoint_modifier = 250,
            ocean_depth = "VERY_DEEP",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_waterdeep_noise",
        },
    },
    OCEAN_SHIPGRAVEYARD = {
        tile_range = TileRanges.SEA,
        tile_data = {
            ground_name = "Ship Grave",
            old_static_id = 105,
        },
        ground_tile_def  = {
            name = "water_medium",
            noise_texture = "ground_water_graveyard",
            flashpoint_modifier = 250,
            ocean_depth = "SHALLOW",
            cannotbedug = true,
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_water_graveyard",
        },
    },

    -------------------------------
    -- IMPASSABLE
    -- (render order doesnt matter)
    -------------------------------

    VOLCANO_LAVA = {
        tile_range = TileRanges.IMPASSABLE,
        tile_data = {
            ground_name = "Lava",
        },
        minimap_tile_def = {
            name = "map_edge",
            noise_texture = "mini_lava_noise",
        },
    },

    -------------------------------
    -- NOISE
    -- (only for worldgen)
    -------------------------------

    VOLCANO_NOISE = {
        tile_range = volcano_noisefn,
    },
}

--Non flooring floodproof tiles
GLOBAL.GROUND_FLOODPROOF = {
    [WORLD_TILES.ROAD] = true,
}

for tile, def in pairs(ia_tiledefs) do
    local range = def.tile_range
    if range == TileRanges.SEA then
        range = TileRanges.LAND --TileRanges.OCEAN
    elseif type(range) == "function" then
        range = TileRanges.NOISE
    end

    AddTile(tile, range, def.tile_data, def.ground_tile_def, def.minimap_tile_def, def.turf_def)

    local tile_id = WORLD_TILES[tile]

    if def.tile_range == TileRanges.SEA then
        --TileGroupManager:AddInvalidTile(TileGroups.TransparentOceanTiles, tile_id) -- add when we merge waters
        TileGroupManager:AddInvalidTile(TileGroups.TEMP_LandTilesNotIAOceanTiles, tile_id) -- remove when we merge waters
        TileGroupManager:AddValidTile(TileGroups.IAOceanTiles, tile_id)
    elseif type(def.tile_range) == "function" then
        NoiseFunctions[tile_id] = def.tile_range
    end
end

for prefab, filter in pairs(GLOBAL.terrain.filter) do
    if type(filter) == "table" then
        table.insert(filter, WORLD_TILES.RIVER)
        table.insert(filter, WORLD_TILES.MANGROVE)
        table.insert(filter, WORLD_TILES.OCEAN_CORAL)
        table.insert(filter, WORLD_TILES.OCEAN_SHALLOW)
        table.insert(filter, WORLD_TILES.OCEAN_MEDIUM)
        table.insert(filter, WORLD_TILES.OCEAN_DEEP)
        table.insert(filter, WORLD_TILES.OCEAN_SHIPGRAVEYARD)
        if table.contains(filter, WORLD_TILES.CARPET) then
            table.insert(filter, WORLD_TILES.SNAKESKIN)
        end
    end
end

SetTileProperty(WORLD_TILES.RIVER_SHORE, "land", false)
SetTileProperty(WORLD_TILES.RIVER, "land", false)
SetTileProperty(WORLD_TILES.MANGROVE_SHORE, "land", false)
SetTileProperty(WORLD_TILES.MANGROVE, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_CORAL_SHORE, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_CORAL, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW_SHORE, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_MEDIUM, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_DEEP, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_SHIPGRAVEYARD, "land", false)

--I want to phase out TILE_TYPE, since I want tiles capable of being both land AND water.
SetTileProperty(WORLD_TILES.RIVER, "land", false)
SetTileProperty(WORLD_TILES.MANGROVE, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_CORAL, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_MEDIUM, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_DEEP, "land", false)
SetTileProperty(WORLD_TILES.OCEAN_SHIPGRAVEYARD, "land", false)

SetTileProperty(WORLD_TILES.RIVER_SHORE, "water", true)
SetTileProperty(WORLD_TILES.RIVER, "water", true)
SetTileProperty(WORLD_TILES.MANGROVE_SHORE, "water", true)
SetTileProperty(WORLD_TILES.MANGROVE, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL_SHORE, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW_SHORE, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_MEDIUM, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_DEEP, "water", true)
SetTileProperty(WORLD_TILES.OCEAN_SHIPGRAVEYARD, "water", true)

--no ground creep
SetTileProperty(WORLD_TILES.RIVER_SHORE, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.RIVER, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.MANGROVE_SHORE, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.MANGROVE, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL_SHORE, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW_SHORE, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_MEDIUM, "groundcreepdisabled", true)
SetTileProperty(WORLD_TILES.OCEAN_DEEP, "groundcreepdisabled", true)
-- register as what SW groups in Map:IsBuildableWater
SetTileProperty(WORLD_TILES.RIVER, "buildable", true)
SetTileProperty(WORLD_TILES.MANGROVE, "buildable", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL, "buildable", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW, "buildable", true)
SetTileProperty(WORLD_TILES.RIVER_SHORE, "buildable", true)
SetTileProperty(WORLD_TILES.MANGROVE_SHORE, "buildable", true)
SetTileProperty(WORLD_TILES.OCEAN_CORAL_SHORE, "buildable", true)
SetTileProperty(WORLD_TILES.OCEAN_SHALLOW_SHORE, "buildable", true)

-- ID 1 is for impassable
--Add after mud because that seems to be a relatively constant "last of cave tiles" and before any high priority turf.
ChangeTileRenderOrder(WORLD_TILES.MEADOW, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.TIDALMARSH, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.MAGMAFIELD, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.SWAMP, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.JUNGLE, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.ASH, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.VOLCANO, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.VOLCANO_ROCK, WORLD_TILES.MUD, true)
ChangeTileRenderOrder(WORLD_TILES.BEACH, WORLD_TILES.MUD, true)
--Priority turf
ChangeTileRenderOrder(WORLD_TILES.SNAKESKIN, WORLD_TILES.CARPET)
--Changing water render order is not advisable, as the
--other tiles can produce visual bugs (grid-shaped bleed-over)
-- ChangeTileRenderOrder(WORLD_TILES.RIVER, 2)
-- ChangeTileRenderOrder(WORLD_TILES.MANGROVE, 2)
-- ChangeTileRenderOrder(WORLD_TILES.OCEAN_CORAL, 2)
-- ChangeTileRenderOrder(WORLD_TILES.OCEAN_SHALLOW, 2)
-- ChangeTileRenderOrder(WORLD_TILES.OCEAN_MEDIUM, 2)
-- ChangeTileRenderOrder(WORLD_TILES.OCEAN_DEEP, 2)
-- ChangeTileRenderOrder(WORLD_TILES.OCEAN_SHIPGRAVEYARD, 2)
--Minimap
local minimap_first
for i, ground in pairs(GroundTiles.minimap) do
    if ground[1] ~= nil then
        minimap_first = ground[1]
        break
    end
end
if minimap_first then
    ChangeMiniMapTileRenderOrder(WORLD_TILES.VOLCANO_LAVA, minimap_first)
end
